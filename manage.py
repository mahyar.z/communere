from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from extensions import db
from application import create_app
from os import getenv
from config import ProductionConfig, UATConfig, DevelopConfig, TestConfig

configuration = ProductionConfig
environment = getenv("ENV") or "PRODUCTION"

if environment == "DEVELOPMENT":
    configuration = DevelopConfig
elif environment == "UAT":
    configuration = UATConfig
elif environment == "TEST":
    configuration = TestConfig

app = create_app(configuration)
migrate = Migrate(app=app, db=db)
manager = Manager(app=app)

manager.add_command("db", MigrateCommand)

if __name__ == "__main__":
    manager.run()
