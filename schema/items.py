import graphene
from graphql import GraphQLError
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from models.users import User as UserModel
from models.items import Item as ItemModel
from extensions import db
from flask import current_app


class Item(SQLAlchemyObjectType):
	class Meta:
		model = ItemModel
		interfaces = (graphene.relay.Node, )


class ItemConnenction(graphene.relay.Connection):
	class Meta:
		node = Item


class CreateItem(graphene.Mutation):
    """Create an item"""
    item = graphene.Field(lambda: Item, description="Item created by this mutation")

    class Arguments:
        username = graphene.String(required=True)
        title = graphene.String(required=True) 
        status = graphene.String(required=True)
        due_date = graphene.DateTime(required=True)
        description = graphene.String()

    def mutate(self, info, username, title, description, status, due_date):
        user = db.session.query(UserModel).filter_by(username=username).first()

        if not user:
            current_app.logger.error("This user does not exist - username: {}".format(username))
            raise GraphQLError('This user does not exist! (username: {})'.format(username))

        item = ItemModel(user_id=user.id,
                         title=title,
                         description=description,
                         status=status,
                         due_date=due_date)

        db.session.add(item)
        db.session.commit()
        current_app.logger.error("Item successfully created - title: {}".format(item.title))

        return CreateItem(item=item)


class UpdateItem(graphene.Mutation):
    """Update an item"""
    item = graphene.Field(lambda: Item, description="Item updated by this mutation")

    class Arguments:
        id = graphene.Int(required=True)
        title = graphene.String(required=True)
        status = graphene.String(required=True)
        due_date = graphene.DateTime(required=True)       
        description = graphene.String()

    def mutate(self, info, id, title, status, due_date, description):
        item = db.session.query(ItemModel).get(id)

        if not item:
            current_app.logger.error("This item does not exist - id: {}".format(id))
            raise GraphQLError('Item does not exists - id: {}'.format(id))

        item.title = title
        item.description = description
        item.status = status
        item.due_date = due_date

        db.session.commit()

        return UpdateItem(item=item)


class DeleteItem(graphene.Mutation):
    message = graphene.String()

    class Arguments:
    	id = graphene.Int(required=True)

    def mutate(self, info, id):
        item = ItemModel.query.get(id)

        if not item:
            current_app.logger.error("This item does not exist - id: {}".format(id))
            raise GraphQLError('Item does not exists')

        db.session.delete(item)
        db.session.commit()
        current_app.logger.info("Item deleted - id: {}".format(id))

        return DeleteItem(message="Item deleted successfully")


class ItemMutation:
    create_item = CreateItem.Field()
    update_item = UpdateItem.Field()
    delete_item = DeleteItem.Field()


class ItemQuery:
    all_items = SQLAlchemyConnectionField(ItemConnenction)
    item = graphene.relay.Node.Field(Item)
    item_by_id = graphene.List(Item, id=graphene.UUID())
    items_by_status = graphene.List(Item, status=graphene.String())
    items_by_due_date = graphene.List(Item, due_date=graphene.DateTime())

    def resolve_item_by_id(self, info, **args):
        id = args.get("id")
        item_query = Item.get_query(info)
        return item_query.get(id)

    def resolve_items_by_status(self, info, **args):
        status = args.get("status")
        item_query = Item.get_query(info)
        items = item_query.filter_by(status=status).all()
        return items

    def resolve_items_by_due_date(self, info, **args):
        due_date = args.get("due_date")
        item_query = Item.get_query(info)
        items = item_query.filter(ItemModel.due_date < due_date).all()
        return items