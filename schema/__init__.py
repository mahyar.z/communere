
import graphene

from .items import ItemQuery, ItemMutation
from .users import UserQuery, UserMutation

class Mutation(ItemMutation, UserMutation, graphene.ObjectType):
	pass

class Query(ItemQuery, UserQuery, graphene.ObjectType):
	node = graphene.relay.Node.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)