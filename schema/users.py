import graphene
from graphql import GraphQLError
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
import uuid
from models.users import User as UserModel
from extensions import db
from flask import current_app


class User(SQLAlchemyObjectType):
	class Meta:
		model = UserModel
		interfaces = (graphene.relay.Node, )


class UserConnection(graphene.relay.Connection):
	class Meta:
		node = User


class CreateOrLoginUser(graphene.Mutation):
    user = graphene.Field(lambda: User, description="User created by this mutation.")

    class Arguments:
        username = graphene.String(required=True)
        
    def mutate(self, info, username):
        user = UserModel.query.filter_by(username=username).first()

        if not user:
            user = UserModel(username=username)
            db.session.add(user)
            db.session.commit()
            current_app.logger.info("User successfully created - username: {}".format(user.username))

        return CreateOrLoginUser(user=user)


class UserMutation:
    create_or_login_user = CreateOrLoginUser.Field()


class UserQuery:
    all_users = SQLAlchemyConnectionField(UserConnection)
    user = graphene.relay.Node.Field(User)
    user_by_username = graphene.List(User, username=graphene.String())

    def resolve_user_by_username(self, info, **args):
        username = args.get("username")
        user_query = User.get_query(info)
        return user_query.filter_by(username=username).all()