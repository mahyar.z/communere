class DefaultConfig(object):
    DEBUG = False
    ENVIRONMENT = "PRODUCTION"
    PORT = 5000
    HOST = "0.0.0.0"
    SQLALCHEMY_DATABASE_URI = "postgresql://localhost:5432/gql?user=gqluser&password=Fg1234nkao"    
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class UATConfig(DefaultConfig):
    PORT = 7777
    ENVIRONMENT = "DEVELOPMENT"


class DevelopConfig(DefaultConfig):
    ENVIRONMENT = "DEVELOPMENT"
    DEBUG=True


class ProductionConfig(DefaultConfig):
    ENVIRONMENT = "PRODUCTION"


class TestConfig(DefaultConfig):
    ENVIRONMENT = "TEST"