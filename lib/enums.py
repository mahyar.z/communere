from enum import Enum


class Status(Enum):
    Done = 'Done'
    InProgress = 'In Progress'