from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

__all__ = ("db", "migrate",)

db = SQLAlchemy()
migrate = Migrate(db=db)