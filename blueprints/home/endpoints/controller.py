from flask import Blueprint
from flask import make_response, jsonify

web = Blueprint("home", __name__)

@web.route("/", methods=["GET"])
def index():
    return make_response("Try using this endpoint /graphql", 200)
