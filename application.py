from flask import Flask, request
from config import ProductionConfig
import logging
from flask_cors import CORS
from flask_graphql import GraphQLView
from schema import schema


def configure_extensions(app):
    import extensions
    for extension in extensions.__all__:
        try:
            getattr(extensions, extension).init_app(app)
        except Exception as e:
            raise Exception('Failed to initialize %s. Reason: %s' %
                            (extension, e))


def configure_logger(app):
    class CustomLogger(logging.Logger):
        def get_user_ip(self):
            try:
                return request.environ.get('HTTP_X_FORWARDED_FOR').split(',')[0] \
                    if request.environ.get('HTTP_X_FORWARDED_FOR') \
                    else request.environ.get('REMOTE_ADDR', request.remote_addr)
            except:
                return ''

        def _log(self, level, msg, args, exc_info=None, extra=None, stack_info=False):
            if extra is None:
                extra = {'ip': self.get_user_ip()}
            super(CustomLogger, self)._log(level, msg, args, exc_info, extra)

    logging.basicConfig(level=logging.DEBUG)
    logging.setLoggerClass(CustomLogger)

    log_formatter = logging.Formatter(
        '[%(name)s] [%(asctime)s] [%(threadName)s %(process)d] [%(levelname)s] '
        '[%(pathname)s] [%(funcName)s] [line:%(lineno)d] '
        '[ip=%(ip)s] [message=%(message)s]')

    file_handler = logging.FileHandler("log/app.log")
    file_handler.setFormatter(log_formatter)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)

    app.logger.addHandler(file_handler)


def configure_blueprints(app):
    from importlib import import_module

    import blueprints

    for blueprint in blueprints.__all__:
        bp = import_module('blueprints.%s' % blueprint, package=blueprint)

        for route in bp.__all__:
            app.register_blueprint(getattr(bp, route))


def configure_graphene(app):
    app.add_url_rule(
		'/graphql', 
		view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True))


def create_app(configuration=None):
    app = Flask(__name__)
    CORS(app, supports_credentials=True)
    app.config.from_object(configuration or ProductionConfig)
    configure_extensions(app)
    configure_blueprints(app)
    configure_logger(app)
    configure_graphene(app)
    return app
