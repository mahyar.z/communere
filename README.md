
## create virtualenv and install requirements
```
virtualenv -p python3 .venv
source .venv/bin/activate
pip install -r requirements.txt
ENV=DEVELOPMENT python3 app.py
```

## migration
init migration
```
ENV=DEVELOPMENT python manage.py db init
```

migrate tables & schema
```
ENV=DEVELOPMENT python manage.py db migrate
ENV=DEVELOPMENT python manage.py db upgrade
```


## Demo
You can try this project on http://78.31.107.65:7777/graphql


## User
create or login user
```
mutation {
  createOrLoginUser(username: "ahmad") {
    user {
      id
      username
      createdAt
    }
  }
}
```

query user with username
```
query {
  userByUsername (username: "mahyar") {
    id
    username
  }
}
```

## Item
Create an item
```
mutation {
  createItem(username: "mahyar", 
            title: "configurations", 
            status: "InProgress", 
            description: "add configuration for all envs", 
            dueDate: "2021-11-20T00:00:00") 
  {
    item {
      id
      title
      status
    }
  }
}
```

Update an item
```
mutation {
  updateItem(id: 1,
             title: "deployment",
             status: "InProgress"
             description: "config docker"
             dueDate: "2021-11-20T00:00:00" ) {
             item {
               id
               title
               status
             }
  }
}
```

Query an item by status
```
query {
  itemsByStatus (status: "InProgress") {
    id
    title,
    description
  }
}
```

Query an item by dueDate (this api will return those items that theirs due date is before [dueDate]!)
```
query {
  itemsByDueDate (dueDate: "2021-11-20T00:00:00") {
    id
    title,
    description
  }
}
```