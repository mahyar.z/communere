from extensions import db
from sqlalchemy.dialects.postgresql import UUID
from datetime import datetime
from lib.enums import Status


class Item(db.Model):
    """ Item """

    __tablename__ = "items"

    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False, autoincrement=True)
    active = db.Column(db.Boolean, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    modified_at = db.Column(db.DateTime, default=datetime.utcnow)

    user_id = db.Column(db.Integer, db.ForeignKey("users.id", ondelete='CASCADE'))
    user = db.relationship("User")

    status = db.Column(db.Enum(Status), nullable=False, index=True)
    title = db.Column(db.String(250), nullable=False)
    description = db.Column(db.String())
    due_date = db.Column(db.DateTime, index=True)

    def __repr__(self):
        return u"<Item:({}, {})>".format(self.id, self.title)

    def serialize(self):
        return {
            "id": self.id,
            "active": self.active,
            "created_at": str(self.created_at),
            "modified_at": str(self.modified_at),

            "user": self.user.serialize(),
            "status": self.status.value,
            "title": self.title,
            "description": self.description or "",
            "due_date": str(self.due_date.strftime("%d/%m/%Y, %H:%M"))
        }