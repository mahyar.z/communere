from extensions import db
from sqlalchemy.dialects.postgresql import UUID
from datetime import datetime


class User(db.Model):
    """ User """

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False, autoincrement=True)
    active = db.Column(db.Boolean, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    modified_at = db.Column(db.DateTime, default=datetime.utcnow)

    username = db.Column(db.String(50), unique=True, nullable=False)

    def __repr__(self):
        return u"<User:({}, {})>".format(self.id, self.username)

    def serialize(self):
        return {
            "id": self.id,
            "active": self.active,
            "created_at": str(self.created_at),
            "modified_at": str(self.modified_at),

            "username": self.username,
        }